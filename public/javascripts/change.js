/**
 * Created by Andrew on 18-Jan-16.
 */
define("change", function() {

  return function() {
    var
      selectedId = $$('tasksTree').getSelectedId(),
      selectedTask = $$('tasksTree').getItem(selectedId);

    if (selectedTask.$level < 2) {
      return;
    }

    $$('codeMission').setValues({
      task: selectedTask.mission,
      description: selectedTask.description
    });
    $$("taskName").setValues({
      name: selectedTask.value
    })
    //delete views from codeBlock
    var block = $$("codeBlocks");
    block.$view.innerHTML = '';
    selectedTask.codeBlocks.forEach(function(block) {

      if (block.editable) {
        $$('codeBlocks').addView({
          view: "textarea",
          origin: block,

          //add fixed height to prevent too small field
          height: 125,
          value: block.data
        });
      } else {
        $$('codeBlocks').addView({
          view: "template",
          template: block.data,
          origin: block,
          autoheight: true
        });
      }
    });

    return selectedTask;
  };
});