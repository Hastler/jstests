/**
 * Created by Andrew on 21-Jan-16.
 */
define("itemClick", function() {

  return function (selectedTask) {
    var
      codeParts = $$('codeBlocks').getChildViews(),
      code = codeParts.reduce(function (result, block) {
        result.push(block.config.origin.editable ?
          block.$getValue() : block.config.origin.data);
        return result;
      }, []);

    if (!selectedTask) return;

    webix.ajax().post('/code', {
      code: code.join('\n'),
      id: selectedTask.id
    }, function (text) {
      $$('codeRunResult').setValues({
        result: text
      });
    });
  }
});
