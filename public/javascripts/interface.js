define(["../libs/webix/webix", "change", "itemClick"], function(w, change, itemClick) {

  return webix.ready(function() {
    var
      selectedTask;

    webix.ui({
      rows:[
        { view:"template",
          type:"header", template:"JS Tests",css: "header" },
        {
          cols: [
            {
              view: "tree",
              id: "tasksTree",
              width: 300,
              select: true,
              url: '/tasks',
              css: "tree",
              on: {
                onSelectChange: function() {
                  selectedTask = change();
                }
              }
            },
            {
              view: "resizer"
            },
            {
              rows: [
                {
                  rows: [
                    {
                      view: "template",
                      id: "taskName",
                      minHeight: 35,
                      css: "task-name",
                      data: {
                        name: "Task name"
                      },
                      template: "<div>#name#</div>"
                    },
                    {
                      view: "template",
                      id: "codeMission",
                      data: {
                        //add default values for fields
                        task: 'Your task will be here',
                        description: 'Description of your task'
                      },
                      autoheight: true,
                      template: "<div>#task#</div><i>#description#</i>"
                    },
                    {
                      rows: [],
                      id: "codeBlocks",
                      height: 200
                    },
                    {
                      view: "form",
                      elements: [
                        {
                          view: "button",
                          value: "Send",
                          autoheight: true,
                          on: {
                            onItemClick: function () {
                              itemClick(selectedTask);
                            }
                          }
                        }
                      ]
                    }
                  ]
                },
                {
                  view: "resizer"
                },
                {
                  view: "template",
                  id: "codeRunResult",
                  css: "code-run",
                  template: '#result#',
                  data: {
                    result: ''
                  }
                }
              ]
            }
          ]
        }
      ]
    });
  });
});