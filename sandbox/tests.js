module.exports = [
  {
    id: "1",
    value: "Arrays",
    data: [
      {
        "id": "1.1",
        "value": "Минимальный элемент",
        "mission": "Найти минимальный элемент массива",
        "description": "Напишите функцию, возвращающую минимальный элемент массива и не используя метод Math.min()",
        "codeBlocks": [
          {
            "data": "function min(arr) {"
          },
          {
            "data": "",
            "editable": true
          },
          {
            "data": "}"
          }
        ],
        tests: [
          {
            sandbox: {},
            testCode: "result = min([34, 2, 6, 17]);",
            result: 2
          },
          {
            sandbox: {},
            testCode: "Math = {}; result =  min([45, 0, 12]);",
            result: 0
          },
          {
            sandbox: {},
            testCode: "result = min([3, 0, 34, -7]);",
            result: -7
          }
        ]
      },
        {
          "id": "1.2",
          "value": "Свертка",
          "mission": "Свернуть массив",
          "description": "Свертка. Используйте метод reduce в комбинации с concat для свёртки массива массивов в один массив, у которого есть все элементы входных массивов.",
          "codeBlocks": [
            {
              "data": "function convolution(arr) {"
            },
            {
              "data": "",
              "editable": true
            },
            {
              "data": "}"
            }
          ],
            tests: [
              {
                sandbox: {},
                testCode: "result = convolution([[1], [2], [3]]);",
                result: [1,2,3]
              },
              {
                sandbox: {},
                testCode: "result = convolution([[1, 2, 3], [4, 5], [6]]);",
                result: [1,2,3,4,5,6]
              },
              {
                sandbox: {},
                testCode: "result = convolution([[1, 2, 3], [4, 5], [6], [10,15]]);",
                result: [1,2,3,4,5,6,10,15]
              }
            ]
        },
        {
          "id": "1.3",
          "value": "Фильтрация массива на 'месте'",
          "mission": "Написать функцию фильтрации массива",
          "description": "Создайте функцию filterRangeInPlace(arr, a, b), которая получает массив с числами arr и удаляет из него все числа вне диапазона a..b. То есть, проверка имеет вид a ≤ arr[i] ≤ b. Функция должна менять сам массив и ничего не возвращать.",
          "codeBlocks": [
            {
              "data": "filterRangeInPlace(arr, a, b) {"
            },
            {
              "data": "",
              "editable": true
            },
            {
              "data": "}"
            }
          ],
            tests: [
              {
                sandbox: {},
                testCode: "result = filterRangeInPlace([5, 3, 8, 1], 1, 4);",
                result: [3,1]
              },
              {
                sandbox: {},
                testCode: "result = filterRangeInPlace([5, 30, 8, 10, 70], 10, 100);",
                result: [10, 30, 70]
              },
              {
                sandbox: {},
                testCode: "result = filterRangeInPlace([5, 3, -1, 15, 7, -10, 8, 1, -5], -5, 7);",
                result: [5, 3, -1, 7, 1, -5]
              }
            ]
        },
        {
          "id": "1.4",
          "value": "Отфильтровать анаграммы",
          "mission": "Написать функцию фильтрации анаграмм",
          "description": "Анаграммы – слова, состоящие из одинакового количества одинаковых букв, но в разном порядке"+
          ".Например, воз - зов, вор - ров, костер - корсет. Напишите функцию aclean(arr), которая возвращает массив слов, очищенный от анаграмм." +
          "Из каждой группы анаграмм должно остаться только одно слово, не важно какое.",
          "codeBlocks": [
            {
              "data": "aclean(arr){"
            },
            {
              "data": "",
              "editable": true
            },
            {
              "data": "}"
            }
          ],
          tests: [
            {
              sandbox: {},
              testCode: "result = aclean(['воз', 'киборг', 'корсет', 'ЗОВ', 'гробик', 'костер', 'сектор']);",
              result: "воз,киборг,корсет" || "ЗОВ,гробик,сектор"
            }
          ]
        }
    ]
  },
  {
    id: "2",
    value: "Date",
    data: [
      {
        "id": "2.1",
        "value": "Валидация даты",
        "mission": "Верно ли задана дата?",
        "description": "Написать функцию принимающую строку с датой в качестве аргумента. Формат даты может быть: YYYY-MM-DD, MM/DD/YYYY или любой другой понимаемый конструктором Date(). Функция должна возвращать true, если дата валидна или false в обратном случае. Необходимо учитывать количество дней в разных месяцах и высокосный год",
        "codeBlocks": [
          {
            "data": "function validateDate(dateStr) {"
          },
          {
            "data": "",
            "editable": true
          },
          {
            "data": "}"
          }
        ],
        "tests": [
          {
            "sandbox": {},
            "testCode": "result = validateDate('02/31/2001');",
            "result": false
          },
          {
            "sandbox": {},
            "testCode": "result = validateDate('1959-04-31');",
            "result": false
          },
          {
            "sandbox": {},
            "testCode": "result = validateDate('1978-02-29');",
            "result": false
          },
          {
            "sandbox": {},
            "testCode": "result = validateDate('1980-02-29');",
            "result": true
          },
          {
            "sandbox": {},
            "testCode": "result = validateDate('02/28/2001');",
            "result": true
          },
          {
            "sandbox": {},
            "testCode": "result = validateDate('31 Feb 2002');",
            "result": false
          },
          {
            "sandbox": {},
            "testCode": "result = validateDate('02/29/01');",
            "result": false
          }
        ]
      }
    ]
  },
    {
      id: "3",
        value: "Object",
        data: [
          {
            "id": "3.1",
            "value": "Пустой объект",
            "mission": "Определите, пустой ли объект.",
            "description": "Напишите функцию, определяющую пустой объект или нет. Функция должна возвращать булево значение.",
            "codeBlocks": [
              {
                "data": "function isEmpty(obj) {"
              },
              {
                "data": "",
                "editable": true
              },
              {
                "data": "}"
              }
            ],
            "tests": [
              {
                "sandbox": {},
                "testCode": "result = isEmpty({x:5});",
                "result": false
              },
              {
                "sandbox": {},
                "testCode": "result = isEmpty({});",
                "result": true
              }
            ]
          }
        ]
    },
    {
      id: "4",
      value: "Numbers",
      data: [
        {
          "id": "4.1",
          "value": "Повтор",
          "mission": "Напишите обертку для функции умножения",
          "description": "Допустим, у вас есть функция primitiveMultiply, которая в 50% случаев перемножает 2 числа, а в остальных случаях выбрасывает исключение типа MultiplicatorUnitFailure. Напишите функцию, обёртывающую эту, и просто вызывающую её до тех пор, пока не будет получен успешный результат. Убедитесь, что вы обрабатываете только нужные вам исключения.",
          "codeBlocks": [
            {//problem with line breaks
              "data": "function MultiplicatorUnitFailure() {}" + "\n function primitiveMultiply(a, b) { \n if (Math.random() < 0.5) \n return a * b; \n else \n throw new MultiplicatorUnitFailure(); \n }\n function reliableMultiply(a, b) {"
            },
            {
              "data": "",
              "editable": true
            },
            {
              "data": "}"
            }
          ],
          "tests": [
            {
              "sandbox": {},
              "testCode": "result = reliableMultiply(8,8);",
              "result": 64
            },
            {
              "sandbox": {},
              "testCode": "result = reliableMultiply(5,5);",
              "result": 25
            },
            {
              "sandbox": {},
              "testCode": "result = reliableMultiply(15,15);",
              "result": 225
            }
          ]
    }
    ]}
];