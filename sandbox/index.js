/**
 * Created by andr on 21.11.15.
 */
'use strict';
var
  vm = require('vm');

module.exports = function(req, res, next) {
  let
    tests = require('./tests'),
    taskId = req.body.id,
    task = tests.reduce((test, section) => test ||
      section.data.reduce((data, prev) => data ||
        prev.id === taskId ? prev : false, false), false),
    success = true,
    error;

  task.tests.forEach(test => {

    let
      sandbox = {};

    Object.assign(sandbox, test.sandbox);

    let
      context = new vm.createContext(sandbox),
      script;

    try {
      script = new vm.Script([req.body.code, test.testCode].join('\n'));
    } catch (e) {
      success = false;
      console.log(e);
      error = 'Incorrect code';
    }

    if (success) {
      try {
        script.runInContext(context);
      } catch (e) {
        success = false;
        error = e.message;
      }
    }

    if (success) {
      if (test.result !== sandbox.result) {
        success = false;
      }
    }
  });

  res.send(success ? 'Ok' : error ? error : 'Фигня');

};